#My Twitter  

###Description  
My Twitter is a  simplified Twitter-like web application that allows users to signup and post.  
###Production deployment  
* Deployed to heroku at https://murmuring-headland-41120.herokuapp.com/   
* Images uploaded by posts are stored in AWS S3 (Simple Storage Service)  
###Acknowledgement  
The application was built based on Ruby on Rails Tutorial: Learn Web Development with Rails by Michael Hartl. (https://www.railstutorial.org/).   

###Add-on feature: edit micropost    
* Access control: only logged-in author can see the edit link of a post. It was done via before action filter in micropost controller.   
* The application will show errors if the edit is unsuccessful, and update the micropost otherwise. 
* This feature was fully tested with unit tests and integration test.