# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
# User.create!(name:  "Example User",
#              email: "example@railstutorial.org",
#              password:              "foobar",
#              password_confirmation: "foobar")
# User.create!(name: "Mai Nguyen",
#              email: "mai.nguyen@gmail.com",
#              password: "mai123",
#              password_confirmation: "mai123",
#              admin: true)

# User.create!(name: "Ben",
#              email: "ben@gmail.com",
#              password: "ben123",
#              password_confirmation: "ben123",
#              admin: true)

# User.create!(name: "Aquinas",
#              email: "aquinas@gmail.com",
#              password: "aquinas123",
#              password_confirmation: "aquinas123")

# 99.times do |n|
#   name = Faker::Name.name
#   email = "example-#{n+1}@railstutorial.org"
#   password = "password"
#   User.create!(name:  name,
#                email: email,
#                password:              password,
#                password_confirmation: password)

users = User.order(:created_at).take(6)
50.times do |n|
  content = Faker::Lorem.sentence(5)
  users.each{ |user| user.microposts.create!(content: content) }

end
