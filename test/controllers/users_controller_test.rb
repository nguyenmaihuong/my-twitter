require 'test_helper'

class UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:mai)
    @another_user = users(:archer)
    @lana = users(:lana)
  end

  test "should get new" do
    get signup_path
    assert_response :success
  end

  test "should redirect edit when not logged in" do
    get edit_user_path(@user)
    assert_not flash.empty?
    assert_redirected_to login_url
  end

  test "should redirect update when not logged in" do
    name  = "Foo Bar"
    email = "foo@bar.com"
    patch user_path(@user), params: {user: {  name: name,
                                              email: email,
                                              password: "",
                                              password_confirmation: ""
                                            } }
    assert_not flash.empty?
    assert_redirected_to login_url                                        
  end

  test "should redirect edit when logged in as wrong user" do
    log_in_as(@another_user)
    get edit_user_path(@user)
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect update when logged in as wrong user" do
    log_in_as(@another_user)
    patch user_path(@user), params: {user: {  name: @user.name,
                                              email: @user.email
                                            } }
    assert flash.empty?
    assert_redirected_to root_url
  end

  test "should redirect to profile page for subsequent logins" do
    get edit_user_path(@user)
    assert_redirected_to login_url
    follow_redirect!
    assert_template 'sessions/new'
    log_in_as(@user)
    follow_redirect!
    assert_template 'users/edit'
    get root_path
    get login_path
    log_in_as(@user)
    follow_redirect!
    assert_template 'users/show'
  end

  test "should redirect index when not logged in" do
    get users_path
    assert_redirected_to login_url
  end

  test "should not allow the admin attribute to be editted via the web " do
    log_in_as(@another_user)
    assert_not @another_user.admin?
    patch user_path(@another_user), params: {user: { password: "abc123",
                                                     password_confirmation: "abc123",
                                                     admin: 1
                                                    } }
    assert_not @another_user.reload.admin?
  end

  test "should redirect to login page when not logged in" do
    assert_no_difference 'User.count' do
      delete user_path(@another_user)
    end
    assert_redirected_to login_url
  end

  test "should redirect to home page when not admin" do
    log_in_as(@another_user)
    assert_no_difference 'User.count' do
      delete user_path(@lana)
    end
    assert_redirected_to root_url
  end
end
