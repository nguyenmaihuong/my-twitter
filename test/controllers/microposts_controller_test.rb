require 'test_helper'

class MicropostsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @micropost = microposts(:orange)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "Lorem ipsum" } }
    end

    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@micropost)
    end

    assert_redirected_to login_url
  end

  test "should redirect destroy for wrong micropost" do
    log_in_as(users(:archer))
    assert_no_difference 'Micropost.count' do
      delete micropost_path(@micropost)
    end
    assert_redirected_to root_url
  end

  test "should redirect edit when not logged in" do
    get edit_micropost_path(@micropost)
    assert_redirected_to login_url
  end

  test "should redirect edit if user is not author" do
    log_in_as(users(:archer))
    get edit_micropost_path(@micropost)
    assert_redirected_to root_url
  end

  test "should redirect update when not logged in" do
    patch micropost_path(@micropost), params: {micropost: {content: "Orange orange!!"}}
    assert_redirected_to login_url
  end

  test "should redirect update is user is not author" do
    log_in_as(users(:archer))
    patch micropost_path(@micropost), params: {micropost: {content: "Orange orange!!"}}
    assert_redirected_to root_url
  end

end
