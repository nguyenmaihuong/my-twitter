require 'test_helper'

class UsersSignupTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "invalid signup information" do
    get signup_path
    assert_select "form[action=?]", "/signup"
    assert_no_difference 'User.count' do
      post users_path, params: { user: {  name: "",
                                          email: "user@invalid",
                                          password: "foo",
                                          password_confirmation: "bar" } }
    end
    assert_template 'users/new'
    # assert_select "a[href=?]", contact_path
    assert_select "div[id=?]", "error_explanation"
    assert_select "div[class=?]", "field_with_errors"
  end

  test "valid signup information" do
    get signup_path
    # before_count = User.count
    assert_difference 'User.count', 1 do
      post signup_path, params: {user: {  name: "abc",
                                          email: "abc@gmail.com",
                                          password: "password",
                                          password_confirmation: "password" } }
    end
    follow_redirect!
    assert_template 'users/show'
    assert_not flash.empty?
    assert is_logged_in?
    # after_count = User.count
    # assert_equal (before_count + 1), after_count
    # assert_equal User.last.name, "mai"
  end

  # failed attempt
  # test "flash should not be empty" do
  #   get signup_path
  #   post signup_path, params: {user: {  name: "mai",
  #                                       email: "mai@gmail.com",
  #                                       password: "mai123",
  #                                       password_confirmation: "mai123" } }    
  #   assert_select "div[class=?]", "alert alert-success"
  # end
end
