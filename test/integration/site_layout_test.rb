require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:mai)
  end

  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    # assert_select "a[href=?]", help_path
    # assert_select "a[href=?]", about_path
    # assert_select "a[href=?]", contact_path

    # get contact_path
    # assert_select "title", full_title("Contact")
  end

  test "layout links for non-logged in users" do
    get root_path
    assert_select "a[href=?]", login_path, count: 1
    assert_select "a[href=?]", users_path, count: 0
  end

  test "layout links for logged in users" do
    get root_path
    log_in_as(@user)
    assert_redirected_to user_path(@user)
    follow_redirect!
    assert_select "a[href=?]", login_path, count: 0
    
    assert_select "a[href=?]", users_path, count: 1
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path, count: 1
    assert_select "a[href=?]", logout_path, count: 1
  end

end
