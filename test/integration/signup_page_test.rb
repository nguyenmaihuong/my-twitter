require 'test_helper'

class SignupPageTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "signup page" do
    get signup_path
    assert_select "title", full_title("Sign up")
  end
end
