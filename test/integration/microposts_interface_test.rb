require 'test_helper'

class MicropostsInterfaceTest < ActionDispatch::IntegrationTest
  def setup
    @user = users(:mai)
  end

  test "micropost_interface" do  
    log_in_as(@user)
    follow_redirect!
    assert_template 'users/show'
    assert_select 'div.pagination'

    # Invalid submission
    get root_path
    assert_select 'input[type=file]'
    assert_no_difference 'Micropost.count' do
      post microposts_path, params: { micropost: { content: "" } }
    end
    assert_select 'div#error_explanation'

    # Valid submission
    content = "first spring"
    picture = fixture_file_upload('test/fixtures/rails2.png', 'image/png')
    assert_difference 'Micropost.count', 1 do
      post microposts_path, params: { micropost: { content: content, picture: picture } }
    end
    micropost = assigns(:micropost) 
    assert micropost.picture?
    assert_redirected_to root_url
    follow_redirect!
    assert_match content, response.body
    
    # Delete post
    assert_select 'a', text: 'delete'
    #the_post = microposts(:orange)
    the_post = @user.microposts.paginate(page: 1).first
    assert_difference 'Micropost.count', -1 do
      delete micropost_path(the_post)
    end
    assert_not flash.empty?
    assert_redirected_to root_url
    follow_redirect!

    # Visit different user (no delete links)
    get user_path(users(:archer))
    assert_select 'a', text: 'delete', count: 0

    # Visit different user (no edit links)
    get user_path(users(:archer))
    assert_select 'a', text: 'edit', count: 0

  end

  test "unsuccessfully edit micropost" do 
    log_in_as(@user)
    follow_redirect!
    get root_path
    the_post = @user.microposts.paginate(page: 1).first
    get edit_micropost_path(the_post)
    patch micropost_path(the_post), params: {micropost: {content: ""}}
    assert_select "div", {:class => "alert", :text => "The form contains 1 error."}
  end

  test "successfully edit micropost" do 
    log_in_as(@user)
    follow_redirect!
    get root_path
    the_post = @user.microposts.paginate(page: 1).first
    get edit_micropost_path(the_post)
    post_new_content = the_post.content + "lah lah lah"
    patch micropost_path(the_post), params: {micropost: {content: post_new_content}}
    follow_redirect!
    assert_match post_new_content, response.body
  end

  test "micropost sidebar count" do
    log_in_as(@user)
    get root_path
    assert_match "#{@user.microposts.count} microposts", response.body
    # User with zero microposts
    other_user = users(:malory)
    log_in_as(other_user)
    get root_path
    assert_match "0 microposts", response.body
    other_user.microposts.create!(content: "A micropost")
    get root_path
    assert_match "1 micropost", response.body
  end

end
