require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @admin = users(:mai)
    @non_admin = users(:archer)
  end

  test "index as admin including pagination and delete links" do
    get login_path
    log_in_as(@admin)
    follow_redirect! 

    get users_path
    assert_template 'users/index'
    # working attempts
    # assert_select "div[class=\"pagination\"]", count: 2
    # assert_select "a[href=?]", users_path + "?page=1", count: 2
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin 
        assert_select 'a[href=?]', user_path(user), method: :delete
      end
    end

    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end

  test "index as non_admin" do 
    log_in_as(@non_admin)
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end

end
