require 'test_helper'

class SessionsHelperTest < ActionView::TestCase
  def setup
    @user = users(:mai)
    remember @user
  end

  # Working attempt to test 'remember branch of current_user'
  # test "current user by cookies"  do
  #   assert current_user.nil?
  #   log_in_as @user
  #   remember @user
  #   session.delete(:user_id)
  #   assert_equal current_user, @user
  # end
    
  test "current user returns right user when session is nil" do
    assert_equal @user, current_user
    assert is_logged_in?
  end

  test "current_user returns nil when remember digest is wrong" do
    @user.update_attribute(:remember_digest, User.digest(User.new_token))
    # assert current_user.nil?
    assert_nil current_user
  end
end
