class SessionsController < ApplicationController

  def new
  end

  def create
    @user = User.find_by(email: params[:session][:email].downcase)
    if @user && @user.authenticate(params[:session][:password])
      # logged user in
      log_in @user
      params[:session][:remember_me] == '1' ? remember(@user) : forget(@user)
      # redirect_to user_url(user)
      redirect_back_or @user
    else
      if @user == nil then
        flash.now[:danger] = "User does not exist"
      else
        flash.now[:danger] = "Invalid email/password combination"
      end
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
